<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "elm/index";
$route['/'] = "elm";
$route['catalog/(:num)'] = "elm/catalog/$1";
$route['catalog/(:num)/page/(:num)'] = "elm/catalog/$1/page/$2";
$route['catalog/(:num)/page'] = "elm/catalog/$1/page";
$route['catalog/(:num)/(:any)/(:any)'] = "elm/catalog/$1/$2/$3";
$route['catalog/(:num)/(:any)/(:any)/page/'] = "elm/catalog/$1/$2/$3/page/";
$route['catalog/(:num)/(:any)/(:any)/page/(:num)'] = "elm/catalog/$1/$2/$3/page/(:num)";

$route['catalog/(:num)/(:num)'] = "elm/catalog/$1/$2";
$route['catalog/(:num)/(:num)/page/(:num)'] = "elm/catalog/$1/$2/page/$3";
$route['catalog/(:num)/(:num)/page'] = "elm/catalog/$1/$2/page/";

$route['product/(:num)'] = "elm/product/$1";
$route['collection/(:num)'] = "elm/collection/$1";

$route['proizvoditeli'] = "elm/proizvoditeli";
$route['proizvoditeli/(:any)'] = "elm/proizvoditeli/$1";
$route['nashi_raboty'] = "elm/nashi_raboty";
$route['contacts'] = "elm/contacts";
$route['partnery'] = "elm/partnery";


$route['svetilnik/(:num)'] = "elm/svetilnik/$1";
$route['404_override'] = '';

//$route['catalog/52'] = "elm/stabilizators";


/* End of file routes.php */
/* Location: ./application/config/routes.php */