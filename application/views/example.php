<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
    <link href="<?=base_url();?>css/style_admin.css" rel="stylesheet" type="text/css" />
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
	font-family: Arial;
	font-size: 14px;
}
a {
    color: blue;
    text-decoration: none;
    font-size: 14px;
}
a:hover
{
	text-decoration: underline;
}
</style>
</head>
<body>


<!--Заголовк-->
<div class="header">
    <div class="header_resize">
        <div class="logo">
            <h1><a href="#"><span>Электро</span>Мир <small>Admin Panel</small></a></h1>
        </div>
        <!--поиск-->
        <div class="searchform">

        </div>
        <div class="clr"></div>
    </div>
    <div class="content">
        <div class="content_resize">

            <div class="mainbar">

                <div style="height:200px;">

                    <div style="margin:10px;padding-left:50px;padding-right:50px;">

                        <div style="color:#666666;border-bottom:3px solid #0066CC;">
                            <h1>Светильники</h1>
                        </div>
                        <div style="float:left;margin-right:20px;padding-left:10px;">
                            <div style="color:#333333;">
                                <h2>Коллекции</h2>
                            </div>

                            <div style="padding-left:50px;">
                                <ul>
                                    <li><a href="new_collection.php">Добавить</a></li>
                                    <li><a href="view_collection.php">Редактировать</a></li>
                                    <li><a href="#">Удалить</a></li>
                                </ul>
                            </div>
                        </div>
                        <div style="float:left;margin-right:20px;padding-left:10px;">

                            <div style="color:#333333;">
                                <h2>Светильники</h2>
                            </div>

                            <div style="padding-left:50px;">
                                <ul>
                                    <li><a href='<?php echo site_url('examples/offices_management')?>'>Редактирование</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

                <div style="height:200px;">
                    <div style="margin:10px;padding-left:50px;padding-right:50px;">

                        <div style="color:#666666;border-bottom:3px solid #0066CC;">
                            <h1>Электротехнические товары</h1>
                        </div>
                        <div style="float:left;margin-right:20px;padding-left:10px;">
                            <div style="color:#333333;">
                                <h2>Каталог</h2>
                            </div>

                            <div style="padding-left:50px;">
                                <ul>
                                    <li><a href="new_catalog.php">Добавить</a></li>
                                    <li><a href="#">Редактировать</a></li>
                                    <li><a href="#">Удалить</a></li>
                                </ul>
                            </div>
                        </div>

                        <div style="float:left;margin-right:20px;padding-left:10px;">

                            <div style="color:#333333;">
                                <h2>Товары</h2>
                            </div>

                            <div style="padding-left:50px;">
                                <ul>
                                    <li><a href='<?php echo site_url('examples/products_management')?>'>Редактирование</a> </li>

                                </ul>
                            </div>
                        </div>

                        <div style="float:left;margin-right:20px;padding-left:10px;">
                            <div style="color:#333333;">
                                <h2>Производители</h2>
                            </div>

                            <div style="padding-left:50px;">
                                <ul>
                                    <li><a href='<?php echo site_url('examples/orders_management')?>'>Редактирование</a></li>

                                </ul>
                            </div>
                        </div>

                    </div>
                </div>




                <!-- <div style="height:200px;"> </div>  -->

                <div class="clr"></div>
            </div>

        </div>
    </div>

</div>
</div>
	<div>
		<a href='<?php echo site_url('examples/customers_management')?>'>Каталог типов товаров</a> |
		<a href='<?php echo site_url('examples/orders_management')?>'>Производители</a> |
		<a href='<?php echo site_url('examples/products_management')?>'>Редактирование</a> |
		<a href='<?php echo site_url('examples/offices_management')?>'>Светильники</a> |
		<a href='<?php echo site_url('examples/employees_management')?>'>Коллекции</a> |

	</div>
	<div style='height:20px;'></div>  
    <div>
		<?php echo $output; ?>
    </div>
</body>
</html>
