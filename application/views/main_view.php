<?php if (!defined('BASEPATH')) exit('No direct script access alloew'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?
        if (!empty($title)){
            echo $title."&nbsp;/Электромир Луганск";}
        else {echo "Электромир Луганск";}
        ?></title>
    <meta name="description" content="<?
                                        if (!empty($meta_d)){
                                            echo $meta_d;}
                                        else {echo "";}
                                      ?>">
    <meta name="keywords" content="<?
                                    if (!empty($meta_k)){
                                        echo $meta_k;}
                                    else {echo "";}
                                    ?>">
	<meta charset=UTF-8 />
	<meta name='yandex-verification' content='57921efaf2c36c54' />
	
        <!-- Add jQuery library -->
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
	
	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="<?=base_url();?>plugins/fancybox/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>plugins/fancybox/jquery.fancybox.css" media="screen" />

	<script type="text/javascript" src="<?=base_url();?>js/gallery.js"></script>
        
        <link href="<?=base_url();?>css/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-27421753-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<!--header-->
<div class="main">
  <!--Лого+поиск-->
  <!--<div style="text-align: center;background:#222; border:2px solid #eee;">
    <h2 style="color:#fff;">Уважаемые посетители наш сайт переехал на новый адрес: <a href="http://luganskelectro.com/">luganskelectro.com/</a></h2>
</div> -->
  <?php $this->load->view('header_view'); ?>
 
  <!--картинка+топ_меню-->
  <?php $this->load->view('top_menu_view'); ?>

  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
            
        <?php
        
                   
        if(isset($cat_img))
        {
        $this->load->view('index_view');
        }
        
        if(isset($product_cat) || isset($svet_cat))
        {

            if($cat_id > 51)
            {
                $this->load->view('product_view');
            }
            else
            {
                $this->load->view('svet_main_view');
            }
        }
        if(isset($item))
        {
            $this->load->view('item_view');
        }
        
        if(isset($contacts))
        {
            $this->load->view('contacts_view');
        }
        if(isset($works))
        {
            $this->load->view('nashi_raboti');
        }
        if(isset($manuf))
        {
            $this->load->view('manufacture_all_view');
        }
        if(isset($man))
        {
            $this->load->view('manufacture_view');
        }
        if(isset($light_list))
        {
            $this->load->view('svet_catalog');
        }
        if(isset($item_light))
        {
            $this->load->view('svet_item_view');
        }
        if(isset($collection))
        {
            $this->load->view('collection_view');
        }
        if(isset($partnery))
        {
            $this->load->view('partners_view');
        }
        ?>
      </div>
        
        <?php $this->load->view('left_menu_view');  ?>
    
      <div class="clr"></div>
    </div>
  </div>
  
  <!-- Галерея -->
  <?php $this->load->view('footer_view'); ?>
 
</div>
</body>
</html>