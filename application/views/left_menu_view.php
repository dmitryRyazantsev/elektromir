<?php if (!defined('BASEPATH')) exit('No direct script access alloew'); ?>
<!--Left colums-->
<div class="sidebar">
    <div class="gadget">

        <?php


        if ((isset($manufacture)  || isset($prod_type)) && (isset($cat_id) && $cat_id >51))
        {
            echo "<h2 class='star'><span>Фильтры</span></h2>";
            echo "<ul class='sb_menu'>";

            $this->load->helper('form');

            echo form_open('catalog/'.$this->uri->segment(2).'/');

            //      Фильтр по производителям
            echo "<h4 style='margin-top:-10px;margin-bottom:-5px;'>Производители</h4>";
            echo "<ul class='menu'>";

            //      Создаем список производителей с
            //      учетом выбранных ранее пунктов

            foreach($manufacture as $row)
            {
                echo "<li style='margin-top:-5px;margin-bottom:-5px;'>";
                //      Выбираем совпавшие элементы

                $c = array_intersect($manufacture_checked,$row);

                if(!empty($c))
                {
                    $data = array(
                        'name' => 'manufacture_id[]',
                        'id' => $row['id'],
                        'value' =>  $row['id'],
                        'checked' => TRUE
                    );
                }
                else
                {
                    $data = array(
                        'name' => 'manufacture_id[]',
                        'id' => $row['id'],
                        'value' =>  $row['id'],
                        'checked' => FALSE
                    );

                }

                echo form_checkbox($data);
                echo form_label($row['title'],$row['id']);

                echo "</li>";

            }
            echo "</ul>";

            //      Фильтр по типам товаров

            echo "<h4 style='margin-top:-5px;margin-bottom:-5px;'>Категории</h4>";
            echo "<ul class='menu'>";
            foreach($prod_type as $row)
            {
                echo "<li style='margin-top:-5px;margin-bottom:-5px;'>";
                //      Выбираем совпавшие элементы
                $c = array_intersect($product_checked,$row);

                if(!empty($c))
                {
                    $data = array(
                        'name' => 'prod_type_id[]',
                        'id' => $row['id'],
                        'value' =>  $row['id'],
                        'checked' => TRUE
                    );
                }
                else
                {
                    $data = array(
                        'name' => 'prod_type_id[]',
                        'id' => $row['id'],
                        'value' =>  $row['id'],
                        'checked' => FALSE
                    );

                }

                echo form_checkbox($data);
                echo form_label($row['title'],$row['id']);

                echo "</li>";

            }
            //   echo form_hidden('ids',$this->uri->segment(3));
            echo form_submit('submit_filter','Выбрать');

            echo form_close();
        }
        echo "</ul>";


        ?>

        <h2 class="star"><span>Каталог</span> товаров</h2>
        <div class="clr"></div>

        <ul class="sb_menu">
            <?php
            //print_r ($svet_cat);
            echo "<ul class='menu'>";
            foreach($menu as $row)
            {
                echo "<li>";
                echo anchor('catalog/'.$row['id'], $row['title']);
                echo "</li>";

                if((isset($svet_cat) && $row['id'] <= 51) || (isset($menu_light) && $row['id'] <= 51))
                {

                    echo "<ul style='list-style-type:circle;margin-left:15px;'>";
                    if(!empty($menu_light))
                    {
                        foreach($menu_light as $row)
                        {
                            if(($row['pid'] == $cat_id && $cat_id<51) || ($row['pid'] == $cat_id_2) || $row['pid'] == $cat_id_2['pid'])
                            {
                                echo "<li style='margin-left:25px;'>";
                                echo anchor('catalog/'.$row['pid'].'/'.$row['id'].'/', $row['title']);
                                echo "</li>";
                            }
                            else
                            {
                                echo "<li style='margin-left:0px;'>";
                                echo anchor('catalog/'.$row['pid'].'/'.$row['id'].'/', $row['title']);
                                echo "</li>";
                            }
                        }
                        echo "</ul>";
                    }
                    else
                    {
                        foreach($svet_cat as $row)
                        {
                            echo "<li>";
                            echo anchor('catalog/'.$row['pid'].'/'.$row['id'].'/', $row['title']);
                            echo "</li>";
                        }
                        echo "</ul>";
                    }
                }
            }
           // echo "</ul>";
            ?>
        </ul>
    </div>

</div>