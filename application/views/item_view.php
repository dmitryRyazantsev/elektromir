<?php if (!defined('BASEPATH')) exit('No direct script access alloew'); ?>
<div>
    <!-- Шапка -->
    <div style='text-align:center;'><h2 style='color:#2B5E9D;'><?=$item['title'];?></h2></div>
    <div style="float: left; width:720px">
        <!-- Картинка -->
        <div style="border:1px solid #B7B7B7;float: left; width:184px; margin-left:50px; margin-right:20px;">
            <?
            $img_small = strstr($item['image_small'],'items');
            $img_full = $item['image_full'];
            if(!$img_small)
            {
                $img_small='items/image_small/'.$item['image_small'];
                $img_full='items/'.$item['image_full'];
            }
            echo anchor($img_full,img(array('src'=>$img_small,'alt'=>$item['title'])),array('title'=>$item['title'],'class'=>'gallery_prod')); ?>
        </div>

        <!-- Цена и артикул -->
        <div style="border:0px solid #B7B7B7; float: left; width:400px;height: 160px; margin-left:10px; margin-right:20px;">
            <div style="margin-top:2px; border-top:1px solid #B7B7B7;border-bottom:1px solid #B7B7B7;width:400px">
                <h3>Техническая характеристика</h3>
            </div>

            <div style='clear: both;'></div>
            <!-- Блок с  Хар -->
            <div style="margin-top:5px; width:420px">
                <!-- Блок с  названиями -->
                <div class="parametrs_name">
                    <div><b>Имя:</b>
                        <?
                        if(strlen($item['title']) > 50)
                        {
                            $count = ceil(strlen($item['title'])/50);
                            for($i = 0; $i < $count; $i++ )
                            {
                                echo "<br>";
                            }
                        }
                        ?>
                    </div>
                    <?php
                    if($item['manufacture']!=0)
                    {
                        echo "<div><b>Производитель:</b></div>";
                    }
                    else
                    {
                        echo "<div>&nbsp;</div>";
                    }
                    ?>

                    <?php
                    if($item['power']!=0)
                    {
                        echo "<div><b>Мощьность:</b></div>";
                    }
                    else
                    {
                        echo "<div>&nbsp;</div>";
                    }

                    ?>
                </div>

                <!-- Блок с  параметрами -->
                <div class="parametrs">
                    <div><? echo wordwrap($item['title'],20,"\n"); ?></div>
                    <div><? if($item['manufacture']!=0){  echo $item['man'];}else{echo "<div>&nbsp;</div>";} ?></div>
                    <div><? if($item['power']!=0){  echo $item['power']."&nbsp;Вт";}else{echo "<div>&nbsp;</div>";} ?></div>
                </div>
                <div style='color:#C03; text-align:left;'>
                    <br><br><br>
                    <h3 ><?php echo "Цена:&nbsp;".$item['price']."&nbsp;грн"; ?></h3>
                </div>

            </div>
        </div>
    </div>
    <div style='clear: both;'></div>

    <!-- Тех Хар -->


    <div style="margin-top:20px; border-top:1px solid #B7B7B7;border-bottom:1px solid #B7B7B7;width:650px">
        <h3>Описание</h3>
    </div>


    <div style='clear: both;'></div>
    <!-- Блок с  Хар -->
    <div style="margin-top:20px; width:720px">
        <!-- Блок с  названиями -->


        <!-- Блок с  параметрами -->
        <div style="float: left; width:600px;text-align:center;">
            <div style="margin-left:120px;margin-right:50px;text-align:justify;"><?=$item['full_description'];?><br><br><br></div>
        </div>

    </div>

</div>