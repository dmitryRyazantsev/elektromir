<?php if (!defined('BASEPATH')) exit('No direct script access alloew'); ?>

<h2 style="text-decoration: none;"><?=$cat_title['title'];?></h2>

<?foreach($product_cat as $row):?>
<div class = "product_main">
    <h2 class = "product_title">
       <? echo anchor('product/'.$row['id'],$cat_title['title_edin']." ".$row['title'],'class="product_title"');
     ?>

    </h2>

    <div class = "product_img">
        <?
        $img_small = strstr($row['image_small'],'items');
        if(!$img_small)
        {
            $img_small='items/image_small/'.$row['image_small'];
        }
        echo anchor('product/'.$row['id'],img(array('src'=>$img_small,'alt'=>$row['title'])));
        ?>
        
    </div>

    <div class = "product_description">
        <p><?=$row['small_description'];?></p>
         <? echo anchor('product/'.$row['id'],'Подробнее...'); ?>
        
    </div>

    <h2 class = "product_price">Цена: <?=$row['price'];?> ГРН</h2>
    </div>
<?endforeach;?>    
<div class="pagination" id="pagination">
    
    <? if(!empty($pages)){echo "<p>Страницы: </p>".$pages;}?>
    <br>
    <br>
</div>
