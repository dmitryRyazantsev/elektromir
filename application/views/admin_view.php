<?php if (!defined('BASEPATH')) exit('No direct script access alloew');?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <?php
    foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
        <?php endforeach; ?>
    <?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
    <style type='text/css'>
        body
        {
            font-family: Arial;
            font-size: 14px;
        }
        a {
            color: blue;
            text-decoration: none;
            font-size: 14px;
        }
        a:hover
        {
            text-decoration: underline;
        }
    </style>
</head>
<body>
<div>
    <a href='<?php echo site_url('admin/manufacturers')?>'>Производители</a> |
    <a href='<?php echo site_url('admin/catalog')?>'>Каталог товара</a> |
    <a href='<?php echo site_url('admin/raboti')?>'>Наши работы</a> |
    <a href='<?php echo site_url('admin/partners')?>'>Партнеры</a> |
    <a href='<?php echo site_url('admin/products')?>'>Товары</a> |
    <a href='<?php echo site_url('admin/produkt_light')?>'>Светильники</a> |
    <a href='<?php echo site_url('admin/collection')?>'>Коллекции</a> |
    <a href='<?php echo site_url('admin/catagories')?>'>Категории</a> |
    <a href='<?php echo site_url('examples/employees_management')?>'>Employees</a> |
    <a href='<?php echo site_url('examples/film_management')?>'>Films</a>
</div>
<div style='height:20px;'></div>
<div>
    <?php echo $output; ?>
</div>
</body>
</html>
