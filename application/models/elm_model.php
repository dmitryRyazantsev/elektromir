<?php if (!defined('BASEPATH')) exit('No direct script access alloew'); 

class Elm_model extends CI_Model
{
//    function __construct()
//    {
//        parent::__construct;
//    }
    
    //Функция для обработки данных для фильтра   
    function filter($ids)
    {
        if(is_array($ids))
        {
            $a = '';
            foreach($ids as $key)
            {
            $a.=$key."-";
            }    
            $a = substr_replace($a,'',-1);
            return $a;
        }
        else 
            return '';
        }
        
    function get_top_menu()
    {
        $q = $this->db->get('top_menu');

        return $q->result_array();
    }

    function get_works()
    {
        $q = $this->db->get('raboti');

        return $q->result_array();
    }

    function get_partners()
    {
        $q = $this->db->get('partners');

        return $q->result_array();
    }

    function get_manufacturers_list()
    {
        $this->db->select('manufacturers.product_type, catalog.title,catalog.title_en');
        $this->db->from('manufacturers');
        $this->db->join('catalog','manufacturers.product_type = catalog.id AND manufacturers.description !=  ""');
        $this->db->group_by('manufacturers.product_type');
        $q = $this->db->get();
        return $q->result_array();
    }
    function get_manufacture($i = NULL)
    {
        if (!isset($i))
        {
        $q = $this->db->query('
                                select id,product_type,title,image,title_en
                                FROM   manufacturers
                                WHERE manufacturers.product_type
                                IN (
                                    select manufacturers.product_type
                                    FROM manufacturers
                                    JOIN catalog ON manufacturers.product_type = catalog.id
                                    AND manufacturers.description !=  "" )
                               ');
            return $q->result_array();
        }
        else
        {
            $this->db->select('title,country,description,image,meta_d,meta_k');
            $this->db->from('manufacturers');
            $this->db->where('title_en',$i);
            $q = $this->db->get();

            if($q->num_rows() > 0)
            {
                $q = $q->row_array();
            }
            return $q;
        }

    }
    function pages()
    {

    }


}
?>
