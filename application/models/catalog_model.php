<?php if (!defined('BASEPATH')) exit('No direct script access alloew');

class Catalog_model extends CI_Model {

//      Получаем корневой каталог
    function get_catalog($i = 0, $id = 0)
    {
        $this->db->select('id,pid,title,title_en,image');
        $this->db->from('catalog');
        if ($i < 50 && $i > 0)
        {
            $ids=array($i,51);
            $this->db->where_in('pid',$ids);
        }
        else
        {
            $this->db->where_in('pid',$i);
        }
        $q = $this->db->get();
        return $q->result_array();
    }
    /*Вывод кнопок с типами товаров*/
    function  get_cat_2($i)
    {
        $this->db->select('id,pid,title,title_en,image');
        $this->db->from('catalog');
        $this->db->where_in('pid',$i);
        $q = $this->db->get();

    return $q->result_array();
    }
//      Получаем список выбранных товаров 

     function get_prod_cat($i,$num = 0, $offset = 0, $prod = NULL, $man= NULL)
{
        //$this->db->select('id,title');
        //$man = "9,10";
        $this->db->where('parrent_cat', $i);
        if (!empty($prod))
        {
            $this->db->where_in('catalog', $prod);
        }
        if (!empty($man))
        {
            $this->db->where_in('manufacture', $man);
        }

        $q = $this->db->get('products', $num, $offset);
        
        return $q->result_array();
    }

//      Количество записей по данному типу товара (для pagination)

    function rows_per_catalog($i, $prod = NULL, $man = NULL ) {  //= NULL
        $this->db->from('products');
        $this->db->where('parrent_cat', $i);
        if (!empty($prod)) {
            $this->db->where_in('catalog', $prod);
        }
        if (!empty($man)) {
            $this->db->where_in('manufacture', $man);
        }
        $q = $this->db->count_all_results();
        return $q;
    }

//      Название корневого каталога
    function catalog_title($i) 
    {
        $this->db->select('id,title,title_edin,title_en');
        $this->db->from('catalog');
        $this->db->where('id', $i);
        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            $r = $q->row_array();
        }
        return $r;
    }

//      Получаем всех производителей для данного типа товара (id)
    function get_manufacture($i)
    {
        $this->db->select('manufacturers.id,manufacturers.title');
        $this->db->from('manufacturers');
        $this->db->join('products', 'manufacturers.id=products.manufacture');
        $this->db->where('parrent_cat', $i);
        $this->db->group_by("manufacture");

        $q = $this->db->get();

        return $q->result_array();
    }

//      Получаем все типы товара для данного типа товара (id)
    function get_prod_type($i)
    {
        $this->db->select('id,title,title_edin');
        $this->db->from('catalog');
        $this->db->where('pid', $i);

        $q = $this->db->get();
        return $q->result_array();
    }

//      Вывод строки с перечьнем типов (транслитом)
    function checked_prod_type($i) 
    {
                 
        $this->db->select('id,title,title_en');
        $this->db->where_in('id',$i);
        $q = $this->db->get('catalog');

        (string)$r = '';

        if ($q->num_rows() > 0) 
        {
            for($c = 0; $c < $q->num_rows(); $c++)
            {
                $row = $q->row_array($c);
                $r.= $row['id'].'-';
            }

            $r = substr_replace($r,'',-1);
        }
      return $r;  
    }

//      Выбор 1го производителя
    function checked_manufacture($i) 
    {
        $this->db->select('manufacturers.id,manufacturers.title');
        $this->db->from('products');
        $this->db->join('manufacturers', 'products.manufacture=manufacturers.id');
        $this->db->where_in('manufacturers.id', $i);
        $this->db->group_by("manufacture");

        $q = $this->db->get();

       (string)$r = '';

        if ($q->num_rows() > 0) 
        {
            for($c = 0; $c < $q->num_rows(); $c++)
            {
                $row = $q->row_array($c);
                $r.= $row['id'].'-';
            }

            $r = substr_replace($r,'',-1);
        }
      return $r;
    }

//      Получсем 1н товар
    function get_item($i) {
        $q = $this->db->query("SELECT products.*,manufacturers.title as man FROM `products` JOIN manufacturers
        ON products.manufacture=manufacturers.id WHERE products.id=$i");

        if($q->num_rows() > 0)
        {
            $q = $q->row_array();
        }
        return $q;
    }

       function item_light_menu($i)
    {
        $q = $this->db->query("select id,pid,title from catalog where pid IN (
        (select pid from catalog where id=(select pid from catalog where id=(select catalog from produkt_light where id=$i))),
        (select id from catalog where id=(select pid from catalog where id=(select catalog from produkt_light where id=$i)))) AND id<51");

        return $q->result_array();
    }

    function item_light_catalog($i)
    {
        $q = $this->db->query("select pid from catalog where id=(select catalog from produkt_light where id=$i)");

        if($q->num_rows() > 0)
        {
            $q = $q->row_array();
        }
        return $q;
    }

//---- Работа со светильниками -----

//      Список классификаций коллекций
    function get_categories()
    {
        $this->db->select('id,title');
        $q = $this->db->get('categories');
        
        return $q->result_array();
    }
    
    function list_prod_light($i,$num = 0, $offset = 0)
    {
        $this->db->select('id,title,image_small');
        $this->db->where('catalog', $i);
        
        $q = $this->db->get('produkt_light', $num, $offset);
        
        return $q->result_array();
    }
    
    function row_list_prod_light($i)
    {
        $this->db->select('id,title,image_small');
        $this->db->from('produkt_light');
        $this->db->where('catalog', $i);
        
        $q = $this->db->count_all_results();
        
        return $q;
    }
    function get_item_light($i) {
        $q = $this->db->query("SELECT produkt_light.*,manufacturers.title as man, collection.title as col FROM `produkt_light`
                                JOIN manufacturers ON produkt_light.manufacture=manufacturers.id
                                JOIN collection ON produkt_light.collection = collection.id
                                WHERE produkt_light.id=$i");
        if($q->num_rows() > 0)
        {
            $q = $q->row_array();
        }
        else
        {
            $q = $this->db->query("SELECT produkt_light.*,manufacturers.title as man FROM `produkt_light`
                                JOIN manufacturers ON produkt_light.manufacture=manufacturers.id
                                WHERE produkt_light.id=$i");
            if($q->num_rows() > 0)
            {
                $q = $q->row_array();
            }
        }

        return $q;
    }
    function get_collection($i)
    {
        $q = $this->db->query("SELECT collection.title,collection.categories,collection.manufacture,collection.image,
                              collection.meta_d,collection.meta_k, manufacturers.title as man, categories.title as cat
                              FROM collection
                              JOIN manufacturers ON manufacturers.id =collection.manufacture
                              JOIN categories ON categories.id = collection.categories  WHERE collection.id= $i");

        if($q->num_rows() > 0)
        {
            $q = $q->row_array();
        }
        return $q;
    }

    function get_collection_items($i)
    {
        $q = $this->db->query("SELECT id,title,image_small,collection FROM produkt_light WHERE collection = $i");

        return $q->result_array();
    }

    
}

