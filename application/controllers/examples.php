<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Examples extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		/* Standard Libraries */
		$this->load->database();
		$this->load->helper('url');
		/* ------------------ */	
		
		$this->load->library('grocery_CRUD');	
	}
	
	function _example_output($output = null)
	{
		$this->load->view('example.php',$output);	
	}
	
	function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}
	
	function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}	
	
	function offices_management()
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('offices');
			$crud->set_subject('Office');
			$crud->required_fields('city');
			$crud->columns('city','country','phone','addressLine1','postalCode');
			
			$output = $crud->render();
			
			$this->_example_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function employees_management()
	{
        $crud = new grocery_CRUD();
            $crud->set_table('top_menu');
			$output = $crud->render();

			$this->_example_output($output);
	}
	
	function customers_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_table('customers');
			$crud->columns('customerName','contactLastName','phone','city','country','salesRepEmployeeNumber','creditLimit');
			$crud->display_as('salesRepEmployeeNumber','from Employeer')
				 ->display_as('customerName','Name')
				 ->display_as('contactLastName','Last Name');
			$crud->set_subject('Customer');
			$crud->set_relation('salesRepEmployeeNumber','employees','{lastName} {firstName}');
			
			$output = $crud->render();
			
			$this->_example_output($output);
	}	
	
	function orders_management()
	{
        $crud = new grocery_CRUD();
        $crud->set_language('russian');
        $crud->set_table('manufacturers');
        $crud->columns('product_type','title','title_en','country','small_description','description','image');
        $crud->fields('product_type','title','title_en','country','small_description','description','image');

        $crud->display_as('title','Название')
            ->display_as('product_type', 'Производит товары')
            ->display_as('title_en','Название латиницей')
            ->display_as('country','Страна')
            ->display_as('small_description', 'Краткое описание')
            ->display_as('description', 'Описание')
            ->display_as('image', 'Логотип');

        $crud->set_relation('product_type','catalog','title');
        $crud->set_field_upload('image','images/manufacturers/');
			$output = $crud->render();
			
			$this->_example_output($output);
	}
	
	function products_management()
	{
			$crud = new grocery_CRUD();
            $crud->set_language('russian');
			$crud->set_table('products');
            $crud->columns('title','parrent_cat','catalog','manufacture','power','small_description','full_description','price','image_small', 'image_full');
            $crud->fields('title','parrent_cat','catalog','manufacture','power','small_description','full_description','price','image_small', 'image_full');
			//$crud->display_as('catalog','Каталог');
			$crud->display_as('title','Название')
                ->display_as('parrent_cat', 'Категория товара')
                ->display_as('catalog','Тип товара')
                ->display_as('manufacture','Производитель')
                 ->display_as('power', 'Мощность')
                 ->display_as('meta_d', 'Мета-описание')
                 ->display_as('meta_k', 'Мета-ключевые слова')
                 ->display_as('small_description', 'Краткое описание')
                 ->display_as('full_description', 'Полное описание')
                 ->display_as('price', 'Цена')
                 ->display_as('image_small', 'Изображение маленькое')
                 ->display_as('image_full', 'Изображение большое')
                 ->display_as('date','Дата добавления');


			$crud->set_relation('manufacture','manufacturers','title');
            $crud->set_relation('catalog','catalog','title');
            $crud->set_relation('parrent_cat','catalog','title');

            //$crud->set_field_upload('image_full','assets/uploads/files');
			//$crud->set_subject('Product');
			//$crud->unset_columns('productDescription');
			//$crud->callback_column('buyPrice',array($this,'valueToEuro'));
			
			$output = $crud->render();
			
			$this->_example_output($output);
	}	
	
	function valueToEuro($value, $row)
	{
		return $value.' &euro;';
	}
	
	function film_management()
	{
		$crud = new grocery_CRUD();
		
		$crud->set_table('menu');
		$crud->set_relation_n_n('actors', 'film_actor', 'actor', 'film_id', 'actor_id', 'fullname','priority');
		$crud->set_relation_n_n('category', 'film_category', 'category', 'film_id', 'category_id', 'name');
		$crud->unset_columns('special_features','description');
		
		$crud->fields('title', 'description', 'actors' ,  'category' ,'release_year', 'rental_duration', 'rental_rate', 'length', 'replacement_cost', 'rating', 'special_features');
		
		$output = $crud->render();
		
		$this->_example_output($output);
	}
	
}