<?php if (!defined('BASEPATH')) exit('No direct script access alloew');
class Elm extends CI_Controller {

  var $catalog_id;
  var $manufacture;
  var $prod_type;
    function index()
    {

        $this->load->model('catalog_model');
        $this->load->model('elm_model');
        $data['top_menu'] = $this->elm_model->get_top_menu();
        $data['cat_img'] = $this->catalog_model->get_catalog();
        $data['menu'] = $this->catalog_model->get_catalog();

        //$data['title'] = "";
        $data['meta_d'] = "светильники, стабилизаторы, автоматические выкоючатели, светодиодная лента,розетки, выключатели,домофоны, видеонаблюдение,охранные системы, автоматика для ворот, счетчики, генераторы, теплый пол, обогреватели, источники бесперебойного питания, пуско-зарядные устройства Электромир Луганск";
        $data['meta_k'] = "светильники, стабилизаторы, автоматические выкоючатели, светодиодная лента,розетки, выключатели,домофоны, видеонаблюдение,охранные системы, автоматика для ворот, счетчики, генераторы, теплый пол, обогреватели, источники бесперебойного питания, пуско-зарядные устройства Электромир Луганск";

        $this->load->view('main_view',$data);

    }
   
    function catalog($id,$cat_id = NULL)
    {
        $this->load->model('catalog_model');
        $this->load->model('elm_model');
        $data['top_menu'] = $this->elm_model->get_top_menu();
        $this->load->library('my_lib');

//      Конфигурация для постраничного вывода

        $config['per_page'] = 10;
        $config['num_links'] = 10;
        $config['first_link'] = 'Первая';
        $config['last_link'] = 'Последняя';
        $config['next_link'] = 'Далее';
        $config['prev_link'] = 'Назад';
        $id = $this->uri->segment(2,52);

//      Обработка светильников

        if($id < 52)
        {
            if(!empty($cat_id) && $id == 51)
            {
                $ids = array($id,$cat_id);
                $data['svet_cat'] = $this->catalog_model->get_cat_2($cat_id);
                $config['base_url'] = 'http://elektromir.lg.ua/catalog/'.$id.'/'.$cat_id.'/page/';
                $config['total_rows'] = $this->catalog_model->row_list_prod_light($cat_id);
                $config['uri_segment'] = 5;
                $config['per_page'] = 12;
                $data['light_list'] = $this->catalog_model->list_prod_light($cat_id,$config['per_page'],$this->uri->segment(5));
                $data['menu_light'] = $this->catalog_model->get_catalog($ids);
                $this->pagination->initialize($config);
            }
            elseif(!empty($cat_id) && $id < 51)
            {
                $config['base_url'] = 'http://elektromir.lg.ua/catalog/'.$id.'/'.$cat_id.'/page/';
                $config['total_rows'] = $this->catalog_model->row_list_prod_light($cat_id);
                $config['uri_segment'] = 5;
                $config['per_page'] = 12;
                $data['light_list'] = $this->catalog_model->list_prod_light($cat_id,$config['per_page'],$this->uri->segment(5));
                $data['menu_light'] = $this->catalog_model->get_catalog($id);
                $this->pagination->initialize($config);

            }
            else
            {
                $data['svet_cat'] = $this->catalog_model->get_catalog($id);

            }
            $data['cat_id_2'] = $this->uri->segment(3);
        }
/* ---ФИЛЬТР по производителям и типам товара и выбор товара--- */
//      Инициализируем переменные из формы (фильтр)     
        
        $prod_type_id = $this->input->post('prod_type_id');
        $manufacture_id = $this->input->post('manufacture_id');
        

if($id >= 52)
{
    $cat_id_all = $this->uri->segment(3);
    $cat_id_p = $this->uri->segment(3);
    $cat_id_m = $this->uri->segment(4);
        if(!empty($prod_type_id) || !empty($manufacture_id) || (!empty($cat_id_all) && $cat_id_all !== "page"))
        {
            if (!empty($manufacture_id) && !empty($prod_type_id))
            {
                $cat_id_all = implode("-",$prod_type_id);
                $man_id = implode("-",$manufacture_id);
                $config['base_url'] = 'http://elektromir.lg.ua/catalog/'.$id.'/'.$cat_id_all.'/'.$man_id.'/page/';
            }

            elseif (!empty($prod_type_id) && empty($manufacture_id))
            {
                $cat_id_p = implode("-",$prod_type_id);
                $config['base_url'] = 'http://elektromir.lg.ua/catalog/'.$id.'/'.$cat_id_p.'/all/page/';
            }

            elseif(!empty($cat_id_p) && $cat_id_m =="all")
            {
                $cat_id_p = $this->uri->segment(3);
                $prod_type_id = explode("-",$cat_id_p);
                $config['base_url'] = 'http://elektromir.lg.ua/catalog/'.$id.'/'.$cat_id_p.'/all/page/';
                $manufacture_id = '';
            }

            elseif (!empty($manufacture_id) && empty($prod_type_id))
            {
                $cat_id_m = implode("-",$manufacture_id);
                $config['base_url'] = 'http://elektromir.lg.ua/catalog/'.$id.'/all/'.$cat_id_m.'/page/';
            }

            elseif (!empty($cat_id_m) && $cat_id_p =="all")
            {
                $manufacture_id = explode("-",$cat_id_m);
                $config['base_url'] = 'http://elektromir.lg.ua/catalog/'.$id.'/all/'.$cat_id_m.'/page/';
                $prod_type_id = '';
            }

            elseif(empty($manufacture_id) && empty($prod_type_id) && $cat_id_all !="page" )
            {
                $man_id = $this->uri->segment(4);
                $prod_type_id = explode("-",$cat_id_all);
                $manufacture_id = explode("-",$man_id);
                $config['base_url'] = 'http://elektromir.lg.ua/catalog/'.$id.'/'.$cat_id.'/'.$man_id.'/page/';
            }

            $config['total_rows'] = $this->catalog_model->rows_per_catalog($id,$prod_type_id,$manufacture_id);
            $config['uri_segment'] = 6;
            $data['product_cat'] = $this->catalog_model->get_prod_cat($id,$config['per_page'],$this->uri->segment(6),$prod_type_id,$manufacture_id);
            $this->pagination->initialize($config);
        }
        else 
        {
            $config['base_url'] = 'http://elektromir.lg.ua/catalog/'.$id.'/page/';
            $config['total_rows'] = $this->catalog_model->rows_per_catalog($id);
            $config['uri_segment'] = 4;
            $config['per_page'] = 10;
            $data['product_cat'] = $this->catalog_model->get_prod_cat($id,$config['per_page'],$this->uri->segment(4));
            $this->pagination->initialize($config);
        }
}
        $data['menu'] = $this->catalog_model->get_catalog();
        if($id < 52)
        {
            if (isset($cat_id))
            {
            $data['cat_title'] = $this->catalog_model->catalog_title($cat_id);
            }
            else
            {
            $data['cat_title'] = $this->catalog_model->catalog_title($id);
            }
        }else
        {
            $data['cat_title'] = $this->catalog_model->catalog_title($id);
        }
        $data['cat_id'] = $id;

        $data['pages'] = $this->pagination->create_links();
        //echo $data['pages']."sdsdsd";
        $data['manufacture'] = $this->catalog_model->get_manufacture($id);
        $data['prod_type'] = $this->catalog_model->get_prod_type($id);

//      Выбранные чек боксы        
        $data['product_checked'] = (!empty($prod_type_id)) ? $prod_type_id : array();
        $data['manufacture_checked'] = (!empty($manufacture_id))? $manufacture_id : array('');
        
        $this->load->view('main_view',$data);
        
    }
    function product($item_id)
    {
         $this->load->model('catalog_model');
         $this->load->model('elm_model');
         $data['top_menu'] = $this->elm_model->get_top_menu();
         $data['menu'] = $this->catalog_model->get_catalog();
         $data['item'] = $this->catalog_model->get_item($item_id);

         $data['title'] = $data['item']['title'];
         $data['meta_d'] = $data['item']['meta_d'];
         $data['meta_k'] = $data['item']['meta_k'];

         $this->load->view('main_view',$data);
     }
     
    function contacts()
     {
        $this->load->model('catalog_model');
        $this->load->model('elm_model');
        $data['top_menu'] = $this->elm_model->get_top_menu();
        $data['menu'] = $this->catalog_model->get_catalog();
        $data['contacts'] = 'contacts';
        $data['title'] = "Контакты";
        $data['meta_d'] = "Контакты Электромир Луганск";
        $data['meta_k'] = "Контакты, Электромир Луганск";
        $this->load->view('main_view',$data);
         
     }
     
    function nashi_raboty()
     {
        $this->load->model('catalog_model');
        $this->load->model('elm_model');
        $data['top_menu'] = $this->elm_model->get_top_menu();
        $data['works'] = $this->elm_model->get_works();
        $data['menu'] = $this->catalog_model->get_catalog();

         $data['title'] = "Наши работы";
         $data['meta_d'] = "Наши работы Электромир Луганск";
         $data['meta_k'] = "Наши работы, Электромир Луганск";

        $this->load->view('main_view',$data);
     }
     
     function proizvoditeli($id = NULL)
     {
        $this->load->model('catalog_model');
        $this->load->model('elm_model');
        $data['top_menu'] = $this->elm_model->get_top_menu();
        $data['menu'] = $this->catalog_model->get_catalog();

         if(!isset($id))
         {
        $data['manuf'] = $this->elm_model->get_manufacturers_list();
        $data['manufacturers'] = $this->elm_model->get_manufacture();
             $data['title'] = "Производители";
             $data['meta_d'] = "Производители Электромир Луганск";
             $data['meta_k'] = "Производители, Электромир Луганск";
         }
         else
         {
             $data['man'] = "man";
             $data['manufacturers'] = $this->elm_model->get_manufacture($id);
             $data['title'] = $data['manufacturers']['title'];
             $data['meta_d'] = $data['manufacturers']['meta_d'];
             $data['meta_k'] = $data['manufacturers']['meta_k'];
         }



        $this->load->view('main_view',$data);
     
     }

     function svetilnik($item_light_id)
     {
         $this->load->model('catalog_model');
         $this->load->model('elm_model');
         $data['top_menu'] = $this->elm_model->get_top_menu();
         $data['menu'] = $this->catalog_model->get_catalog();
         $data['item_light'] = $this->catalog_model->get_item_light($item_light_id);
         $data['menu_light'] = $this->catalog_model->item_light_menu($item_light_id);
         $data['cat_id'] = 51;
         $data['cat_id_2'] = $this->catalog_model->item_light_catalog($item_light_id);
         $data['title'] = $data['item_light']['title'];
         $data['meta_d'] = $data['item_light']['meta_d'];
         $data['meta_k'] = $data['item_light']['meta_k'];
         //var_dump($data['item_light']);
         $this->load->view('main_view',$data);
         
     }
    function collection($collection_id)
    {
        $this->load->model('catalog_model');
        $this->load->model('elm_model');
        $data['top_menu'] = $this->elm_model->get_top_menu();
        $data['menu'] = $this->catalog_model->get_catalog();
        $data['collection'] = $this->catalog_model->get_collection($collection_id);
        $data['collection_items'] = $this->catalog_model->get_collection_items($collection_id);
        $data['title'] = $data['collection']['title'];
        $data['meta_d'] = $data['collection']['meta_d'];
        $data['meta_k'] = $data['collection']['meta_k'];

        //var_dump($data['collection']);
        //echo $data['collection']['man'];

        $this->load->view('main_view',$data);
    }
     function partnery()
     {
         $this->load->model('catalog_model');
         $this->load->model('elm_model');
         $data['top_menu'] = $this->elm_model->get_top_menu();
         $data['menu'] = $this->catalog_model->get_catalog();
         $data['partnery'] = $this->elm_model->get_partners();

         $data['title'] = "Партнеры";
         $data['meta_d'] = "Партнеры Электромир Луганск";
         $data['meta_k'] = "Партнеры, Электромир Луганск";

         $this->load->view('main_view',$data);


     }
}

?>