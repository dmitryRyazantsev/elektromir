<?php if (!defined('BASEPATH')) exit('No direct script access alloew');
class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        /* Standard Libraries */
        $this->load->database();
        $this->load->helper('url');
        /* ------------------ */

        $this->load->library('grocery_CRUD');
    }

    function index()
    {
        $this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
    }
    function manufacturers()
    {
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_language('russian');
        $crud->set_table('manufacturers');
        $crud->columns('product_type','title','title_en','country','small_description','description','image');
        $crud->fields('product_type','title','title_en','country','small_description','description','image');

        $crud->display_as('title','Название')
            ->display_as('product_type', 'Производит товары')
            ->display_as('title_en','Название латиницей')
            ->display_as('country','Страна')
            ->display_as('small_description', 'Краткое описание')
            ->display_as('description', 'Описание')
            ->display_as('image', 'Логотип');

        $crud->set_relation('product_type','catalog','title');
        $crud->set_field_upload('image','images/manufacturers/');

        $crud->callback_after_upload(array($this,'manufacture_callback_after_upload'));

        $output = $crud->render();

        $this->_example_output($output);
    }
    function manufacture_callback_after_upload($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');

        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;

        $this->image_moo->load($file_uploaded)->stretch(160,50)->save($file_uploaded,true);

        return true;
    }

    function raboti()
    {
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_language('russian');
        $crud->set_table('raboti');
        $crud->columns('title','img_small','img_full');
        $crud->fields('title','img_small','img_full');

        $crud->display_as('title','Название')
            ->display_as('img_small', 'Миниатюра')
            ->display_as('img_full', 'Изображение');

        //$crud->set_relation('product_type','catalog','title');
        $crud->set_field_upload('img_small','images/nashi_raboti/');

        $crud->callback_after_upload(array($this,'raboti_callback_after_upload'));

        $crud->set_field_upload('img_full','images/nashi_raboti/');
        $output = $crud->render();

        $this->_example_output($output);
    }

    function raboti_callback_after_upload($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');

        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;

        $this->image_moo->load($file_uploaded)->stretch(180,180)->save($file_uploaded,true);

        return true;
    }

    function partners()
    {
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_language('russian');
        $crud->set_table('partners');
        $crud->columns('title','description','image','link');
        $crud->fields('title','description','image','link');

        $crud->display_as('title','Название')
            ->display_as('description', 'Описание')
            ->display_as('image', 'Миниатюра')
            ->display_as('link', 'Адресс');

        $crud->set_field_upload('image','images/partnery/');

        $crud->callback_after_upload(array($this,'partners_callback_after_upload'));

        $output = $crud->render();

        $this->_example_output($output);
    }

    function partners_callback_after_upload($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');

        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;

        $this->image_moo->load($file_uploaded)->stretch(188,135)->save($file_uploaded,true);

        return true;
    }

    function products()
    {
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_language('russian');
        $crud->set_table('products');
        $crud->columns('title','parrent_cat','catalog','manufacture','power','small_description','full_description','price',
            'image_small','image_full');
        $crud->fields('title','parrent_cat','catalog','manufacture','power','small_description','full_description','price',
            'image_small','image_full');

        $crud->display_as('title','Название')
            ->display_as('parrent_cat', 'Вид товара')
            ->display_as('catalog', 'Тип товара')
            ->display_as('manufacture', 'Производитель')
            ->display_as('power', 'Мощьность')
            ->display_as('small_description', 'Краткое описание')
            ->display_as('full_description', 'Описание')
            ->display_as('price', 'Цена')
            ->display_as('image_small', 'Миниатюра')
            ->display_as('image_full', 'Изображение')
            ->display_as('link', 'Адресс');

        $crud->set_relation('catalog','catalog','title');
        $crud->set_relation('parrent_cat','catalog','title');
        $crud->set_relation('manufacture','manufacturers','title');

        $crud->set_field_upload('image_small','items/image_small');
        $crud->callback_after_upload(array($this,'products_callback'));
        $crud->set_field_upload('image_full','items');
        $output = $crud->render();

        $this->_example_output($output);
    }

    function products_callback($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');

        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
        if($field_info->upload_path =='items/image_small')
        {
        $this->image_moo->load($file_uploaded)->stretch(178,150)->save($file_uploaded,true);
        }
        return true;
    }
    function produkt_light()
    {
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_language('russian');
        $crud->set_table('produkt_light');
        $crud->columns( 'title','meta_d', 'meta_k', 'categories', 'collection', 'catalog', 'parrent_cat', 'article', 'manufacture', 'height', 'width', 'quantity_lamp', 'patron_type', 'power', 'color', 'material', 'catalog_page', 'price', 'description', 'image_small', 'image_full');
        $crud->fields('title','meta_d', 'meta_k', 'categories', 'collection', 'catalog', 'parrent_cat', 'article', 'manufacture', 'height', 'width', 'quantity_lamp', 'patron_type', 'power', 'color', 'material', 'catalog_page', 'price', 'description', 'image_small', 'image_full');

        $crud->display_as('title','Название')
            ->display_as('parrent_cat', 'Вид товара')
            ->display_as('catalog', 'Тип товара')
            ->display_as('manufacture', 'Производитель')
            ->display_as('power', 'Мощьность')
            ->display_as('height', 'Высота')
            ->display_as('width', 'Ширина')
            ->display_as('article', 'Артикул')
            ->display_as('color', 'Цвет')
            ->display_as('material', 'Материал')
            ->display_as('patron_type', 'Тип патрона')
            ->display_as('quantity_lamp', 'Количество ламп')
            ->display_as('full_description', 'Описание')
            ->display_as('price', 'Цена')
            ->display_as('categories', 'Категория')
            ->display_as('collection', 'Коллекция')
            ->display_as('catalog', 'Тип товара')
            ->display_as('parrent_cat', 'Вид товара')
            ->display_as('image_small', 'Миниатюра')
            ->display_as('image_full', 'Изображение');

        $crud->set_relation('catalog','catalog','title');
        $crud->set_relation('parrent_cat','catalog','title');
        $crud->set_relation('manufacture','manufacturers','title');
        $crud->set_relation('collection','collection','title');
        $crud->set_relation('categories','categories','title');

        $crud->set_field_upload('image_small','items/image_small');
        $crud->callback_after_upload(array($this,'products_callback'));
        $crud->set_field_upload('image_full','items');
        $output = $crud->render();

        $this->_example_output($output);
    }

    function produkt_light_callback($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');

        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
        if($field_info->upload_path =='items/image_small')
        {
        $this->image_moo->load($file_uploaded)->stretch(178,150)->save($file_uploaded,true);
        }
        return true;
    }
    function categories()
    {
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_language('russian');
        $crud->set_table('categories');
        $crud->columns('title','description','meta_d','meta_k','image');
        $crud->fields('title','description','meta_d','meta_k','image');

        $crud->display_as('title','Название')
            ->display_as('description', 'описание')
            ->display_as('image', 'Миниатюра');

        $crud->set_field_upload('image','images/categories');
        $output = $crud->render();

        $this->_example_output($output);
    }
    function catalog()
    {
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_language('russian');
        $crud->set_table('catalog');
        $crud->columns('pid', 'title', 'title_edin', 'title_en',  'description');
        $crud->fields('pid', 'title', 'title_edin', 'title_en',  'description');

        $crud->display_as('title','Название')
            ->display_as('pid', 'Родительский тип товара')
            ->display_as('title_edin', 'Заголовок в единственном числе')
            ->display_as('title_en', 'Заголовок латинскими буквами')
            ->display_as('description', 'описание');

        //$crud->set_relation('pid','catalog','title');
                $output = $crud->render();

        $this->_example_output($output);
    }

    function collection()
    {
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_language('russian');
        $crud->set_table('collection');
        $crud->columns('title','manufacture','categories','image');
        $crud->fields('title','manufacture','categories','image');

        $crud->display_as('title','Название')
            ->display_as('manufacture', 'Производитель')
            ->display_as('categories', 'Категория')
            ->display_as('image', 'Миниатюра');

        $crud->set_relation('categories','categories','title');
        $crud->set_relation('manufacture','manufacturers','title');

        $crud->set_field_upload('image','items/svetilniki/classic/collection/');
        $crud->callback_after_upload(array($this,'collection_callback'));
        $output = $crud->render();

        $this->_example_output($output);
    }

    function collection_callback($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');

        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
        if($field_info->upload_path =='items/svetilniki/classic/collection/img_small')
        {
            $this->image_moo->load($file_uploaded)->stretch(375,490)->save($file_uploaded,true);
        }
        return true;
    }

    function _example_output($output = null)

    {
        $this->load->view('admin_view.php',$output);
    }
}